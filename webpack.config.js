module.exports = {
  mode: 'production',
  entry: './src/index.js',
  output: {
    path: __dirname + '/web/js',
  },
};
