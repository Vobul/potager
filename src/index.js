import { jsPDF } from "jspdf";

function makePdf() {
  const nameInput = document.getElementById('name');
  const name = nameInput.value;
  const colorInput = document.getElementById('color');
  const color = colorInput.value;

  const doc = new jsPDF();
  doc.text(30, 50, `Nom : ${name}`, null, 45);
  doc.text(30, 70, `Couleur : ${color}`, null, 45);
  doc.save("out.pdf");
}

document.addEventListener('DOMContentLoaded', () => {
  const button = document.querySelector('[data-action="make-pdf"]');
  button.addEventListener('click', () => makePdf());
});
